//
//  ViewRouter.swift
//  Поиск Мастеров
//
//  Created by Lonedone on 11/07/2017.
//  Copyright © 2017 BarMarket. All rights reserved.
//

import Foundation
import UIKit

//  App routing

final class App: NSObject {
    
    static func start() {
        showFirst()
    }
    
    static func firstControllerDidHisJob() {
        showSecond()
    }
    
    static func secondControllerDidHisJob() {
        showFirst()
    }

    ///  Tab Bar Methods

    static func firstSelected() {
        showFirst()
    }

    static func secondSelected() {
        showSecond()
    }

    //  end
    
    //  Common Methods
    
//    static func beginLoading() {
//        SVProgressHUD.setDefaultMaskType(.gradient)
//        SVProgressHUD.setBackgroundColor(UIColor.blue.withAlphaComponent(0.65))
//        SVProgressHUD.setForegroundColor(UIColor.white)
//        SVProgressHUD.show()
//    }
//    
//    static func finishLoading() {
//        SVProgressHUD.dismiss()
//    }
    
//    static func showError(error: Error?) {
//        if error != nil {
//            showDropDownError(withText: error?.localizedDescription ?? ErrorDB.unknownError.localizedDescription)
//        }
//    }
//    
//    static func showWarning(_ error: Error?) {
//        if error != nil {
//            showDropDownWarning(withText: error?.localizedDescription ?? ErrorDB.unknownError.localizedDescription)
//        }
//    }
    
//    static func showDropDownError(withText text: String) {
//        RKDropdownAlert.title(
//            Messages.error,
//            message: text,
//            backgroundColor: UIColor.red,
//            textColor: UIColor.white
//        )
//    }
//    
//    static func showDropDownWarning(withText text: String) {
//        RKDropdownAlert.title(
//            Messages.warning,
//            message: text,
//            backgroundColor: UIColor.blue,
//            textColor: UIColor.white
//        )
//    }
    
//    static func showConfirmation(title: String = Messages.confirmation, message: String, declined: (() -> ())?, confirmed: @escaping () -> ()) {
//        ConfirmationUI.alert(title: title, message: message, rootView: App.mainController, declined: nil, confirmed: confirmed)
//    }
    
    //  Common Methods End
    
    //  Push Methods
    
//    static func acceptencePushFor(order: Order) {
//        showOrders(status: .accepted)
//        showOrderDetails(order: order)
//    }
//    
//    static func archived(order: Order) {
//        showOrders(status: .archived)
//        showOrderDetails(order: order)
//    }
    
    //  Push Methods End
}

//  View casting methods
extension App {
    
    static fileprivate func showFirst() {
        mainController.hideTabBar()
        let login = FirstView(nibName: "FirstView", bundle: nil)
        App.mainController.changeMainViewController(login, close: true)
    }
    
    static fileprivate func showSecond() {
        mainController.showTabBar()
        let login = SecondView(nibName: "SecondView", bundle: nil)
        App.mainController.changeMainViewController(login, close: true)
    }
}



//  Support methods
extension App {
    
    static fileprivate var mainController: MainMenuController {
        get {
            if let main = UIApplication.shared.keyWindow?.rootViewController as? MainMenuController {
                return main
            } else {
                let mainViewController = EntryView(nibName: "EntryView", bundle: nil)
                let tabbarViewController = TabBarMenu(nibName: "TabBarMenu", bundle: nil)
                let leftSlideViewController = LeftSlideMenu(nibName: "LeftSlideMenu", bundle: nil)
                let rightSlideViewController = RightSlideMenu(nibName: "RightSlideMenu", bundle: nil)
                let mainMenuController = MainMenuController(mainViewController: mainViewController, leftMenuViewController: leftSlideViewController, rightMenuViewController: rightSlideViewController, tabbarViewController: tabbarViewController)
                UIApplication.shared.keyWindow?.rootViewController = mainMenuController
                return mainMenuController
            }
        }
    }
    
    static fileprivate var navigationController: UINavigationController? {
        if let main = App.mainController.mainViewController as? UINavigationController {
            return main
        }
        return nil
    }
    
    static fileprivate func addToNavigationStack(viewController: UIViewController) {
        if let main = navigationController {
            main.pushViewController(viewController, animated: true)
        } else {
            let nav = UINavigationController(rootViewController: viewController)
            App.mainController.changeMainViewController(nav, close: true)
        }
    }
    
    static func setupMenu(appDele: AppDelegate) {
        appDele.window = UIWindow(frame: UIScreen.main.bounds)
        let mainMenuController = mainController
        appDele.window?.rootViewController = mainMenuController
        appDele.window?.makeKeyAndVisible()
    }
}
