//
//  SlideMenuController.swift
//  Discount_System_iOS
//
//  Created by Lonedone on 24/01/17.
//  Copyright © 2017 appcraft. All rights reserved.
//

import Foundation
import UIKit

extension MainMenuController {
    static func showYourself () -> MainMenuController {
        let mainViewController = EntryView(nibName: "EntryView", bundle: nil)
        let tabbarViewController = TabBarMenu(nibName: "TabBarMenu", bundle: nil)
        let mainMenuController = MainMenuController(mainViewController: mainViewController, tabbarViewController: tabbarViewController)
        UIApplication.shared.keyWindow?.rootViewController = mainMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
        return mainMenuController
    }
}
