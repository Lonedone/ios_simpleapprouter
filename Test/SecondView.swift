//
//  PreLoginView.swift
//  Поиск Мастеров
//
//  Created by Lonedone on 18/05/17.
//  Copyright © 2017 BarMarket. All rights reserved.
//
//  TO DO: verify при входе, в том числе перед регформой
//  TO DO: дважды вызывается isLoggedIn

import Foundation
import UIKit

class SecondView: UIViewController {
    
    var presenter: SecondPresenter?
    
    override func viewDidLoad() {
        presenter = SecondPresenter(view: self, service: SecondService())
    }
    
    @IBAction func buttonTouched(_ sender: Any) {
        App.secondControllerDidHisJob()
    }
    

}
