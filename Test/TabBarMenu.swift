//
//  TabBarMenu.swift
//  Поиск Мастеров
//
//  Created by Lonedone on 11/07/2017.
//  Copyright © 2017 BarMarket. All rights reserved.
//

import Foundation
import UIKit

class TabBarMenu: UIViewController {

    @IBAction func firstTouched(_ sender: Any) {
        App.firstSelected()
    }
    
    @IBAction func secondTouched(_ sender: Any) {
        App.secondSelected()
    }
    
}
