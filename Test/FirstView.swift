//
//  PreLoginView.swift
//  Поиск Мастеров
//
//  Created by Lonedone on 18/05/17.
//  Copyright © 2017 BarMarket. All rights reserved.
//
//  TO DO: verify при входе, в том числе перед регформой
//  TO DO: дважды вызывается isLoggedIn

import Foundation
import UIKit

class FirstView: UIViewController {
    
    var presenter: FirstPresenter?
    
    @IBAction func buttonTouched(_ sender: Any) {
        App.firstControllerDidHisJob()
    }
    
    override func viewDidLoad() {
        presenter = FirstPresenter(view: self, service: FirstService())
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        if AccountManager.shared.isLoggedIn {
//            presenter?.loadData {
//                self.start()
//            }
//        } else {
//            start()
//        }
    }
    
    func start() {
//        App.start()
    }

}
