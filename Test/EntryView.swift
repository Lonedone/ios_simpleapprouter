//
//  PreLoginView.swift
//  Поиск Мастеров
//
//  Created by Lonedone on 18/05/17.
//  Copyright © 2017 BarMarket. All rights reserved.
//
//  TO DO: verify при входе, в том числе перед регформой
//  TO DO: дважды вызывается isLoggedIn

import Foundation
import UIKit

class EntryView: UIViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.seconds(2)) { 
            App.start()
        }
    }
}
